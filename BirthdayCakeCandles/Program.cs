﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
class Solution
{

    static void Main( String[] args )
    {
        var n = Convert.ToInt32( Console.ReadLine() );

        var height_temp = Console.ReadLine()?.Split( ' ' );

        var height = Array.ConvertAll( height_temp, Int32.Parse );

        // Keep track of the current max height
        var max_height = height.Max();

        var ocurrences = height.Where( x => x == max_height ).Count();

        // Then increment each time it's seen again or else if a new one shows up restart count

        Console.WriteLine( ocurrences );
    }
}
