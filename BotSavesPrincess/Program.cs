﻿using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System;

public class Solution
{
    /* Head ends here */
    static void displayPathtoPrincess(int n, string[] grid)
    {
        int princessX = 0;
        int princessY = 0;

        // Determine princess' location
        for (int i = 0; i < grid.Length; i++)
        {
            if (grid[i].Contains("p"))
            {
                princessY = i;
                princessX = grid[i].IndexOf("p");
                break;
            }
        }

        // Calculate path based on princess' location
        var botPosition = n / 2;

        // Horizontal movement
        var horizMovement  = princessX - botPosition; // horiz distance from center of grid
        var horizDirection = Math.Sign(horizMovement) == -1 ? LEFT
                           : Math.Sign(horizMovement) ==  1 ? RIGHT 
                           : NONE;

        for (int i = 0; i < Math.Abs(horizMovement); i++)
        {
            Console.WriteLine(horizDirection);
        }

        // Vertical movement
        var verticalMovement  = princessY - botPosition;
        var verticalDirection = Math.Sign(verticalMovement) == -1 ? UP : Math.Sign(verticalMovement) == 1 ? DOWN : NONE;

        for (int i = 0; i < Math.Abs(verticalMovement); i++)
        {
            Console.WriteLine(verticalDirection);
        }

    }

    const string LEFT = "LEFT";

    const string NONE = "NONE";

    const string RIGHT = "RIGHT";

    const string UP = "UP";

    const string DOWN = "DOWN";

    /* Tail starts here */
    static void Main(string[] args)
    {
        int size;

        size = int.Parse(Console.ReadLine());


        string[] grid = new string[size];
        for (int i = 0; i < size; i++)
        {
            grid[i] = Console.ReadLine();

        }

        displayPathtoPrincess(size, grid);
    }
}