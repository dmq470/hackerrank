﻿using System;
using System.Diagnostics;

namespace BotSavesPrincess2
{
    class Solution
    {
        static void nextMove(int n, int r, int c, String[] grid)
        {
            int princessX = 0;
            int princessY = 0;

            // Determine princess' location
            for (int i = 0; i < grid.Length; i++)
            {
                if (grid[i].Contains("p"))
                {
                    princessY = i;
                    princessX = grid[i].IndexOf("p");
                    break;
                }
            }

            // Calculate path based on princess' location

            // Horizontal movement
            var botPosition = c;
            var horizMovement = princessX - botPosition; // horiz distance from center of grid
            var horizDirection = Math.Sign(horizMovement) == -1 ? LEFT
                               : Math.Sign(horizMovement) == 1 ? RIGHT
                               : NONE;

            if (horizDirection != NONE)
            {
                log(horizDirection);
                return;
            }

            // Vertical movement
            botPosition = r;
            var verticalMovement = princessY - botPosition;
            var verticalDirection = Math.Sign(verticalMovement) == -1 ? UP : Math.Sign(verticalMovement) == 1 ? DOWN : NONE;

            log(verticalDirection);
        }

        const string NONE  = "NONE";
        const string LEFT  = "LEFT";
        const string RIGHT = "RIGHT";
        const string UP    = "UP";
        const string DOWN  = "DOWN";

        static void Main(string[] args)
        {
            int dim = int.Parse(Console.ReadLine());

            var pos = Console.ReadLine();

            string[] position = pos.Split(' ');

            int[] int_pos = new int[2];
            int_pos[0] = Convert.ToInt32(position[0]);
            int_pos[1] = Convert.ToInt32(position[1]);

            string[] grid = new String[dim];
            for (int i = 0; i < dim; i++)
            {
                grid[i] = Console.ReadLine();
            }

            nextMove(dim, int_pos[0], int_pos[1], grid);
        }

        private static void log(string msg)
        {
            if (Debugger.IsAttached)
            {
                Trace.WriteLine(msg);
            }
            else
                Console.WriteLine(msg);
        }
    }
}
