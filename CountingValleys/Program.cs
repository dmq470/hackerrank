﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountingValleys
{
    class Program
    {
        static void Main( string[] args )
        {
            var n = Convert.ToInt32( Console.ReadLine() );

            var steps = Console.ReadLine()?.ToCharArray();

            if ( steps == null || steps.Length == 0 ) return;

            // Need to keep track when away from sea level (0, -1, +1)

            var in_valley = steps[0] == 'D';

            var altitude = in_valley ? -1 : 1;

            var num_valleys = 0;

            foreach ( var step in steps.Skip( 1 ) )
            {
                switch ( step )
                {
                    case 'U':
                        altitude++;

                        if ( in_valley && altitude == 0 )
                            num_valleys++;

                        break;
                    default:
                        altitude--;
                        break;
                }

                in_valley = altitude < 0;

            }

            Console.WriteLine( num_valleys );
        }
    }
}

