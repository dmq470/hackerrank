﻿using System;
using System.Diagnostics;
using System.IO;

namespace LisasWorkbook
{

    /*
Input: 
95 3
9 1 1 81 16 1 100 89 100 100 100 2 100 3 100 3 100 100 100 100 100 100 100 100 100 100 100 100 100 100 1 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 100 2 100 100 100 1 100 100 100 100 100 100 100 1 100 100 100 100 100 100 100 100 100 100 100 100 2 100 100 3 100 100 100 100 3 100 100 100 100 100 100 100 100 100 100 100 100 100 100

Output: 
3
    */
    class Solution
    {
        static void Main(string[] args)
        {
            var info_str = Console.ReadLine();
            var info_arr = info_str?.Split(' ');

            var num_chapters       = int.Parse( info_arr ? [0] ?? "" );
            var max_probs_per_chap = int.Parse( info_arr ? [1] ?? "" );

            //Console.WriteLine( $"n: {num_chapters} k: {max_probs_per_chap}" );
            if (num_chapters > 100 || max_probs_per_chap > 100 || num_chapters < 1 || max_probs_per_chap < 1)
                return;


            byte[] inputBuffer = new byte[1024];

            Stream inputStream = Console.OpenStandardInput(inputBuffer.Length);

            Console.SetIn(new StreamReader(inputStream, Console.InputEncoding, false, inputBuffer.Length));
             
            var num_problems_str = Console.ReadLine();

            Console.WriteLine( num_problems_str );

            if (string.IsNullOrEmpty(num_problems_str)) return;

            var num_problems_arr = new int[num_chapters];

            var num_probs_str_arr = num_problems_str.Split( new[] {' '},
                                                            StringSplitOptions.RemoveEmptyEntries);

        
            for (var i = 0; i < num_probs_str_arr.Length; i++)
            {
                var chap_probs = Convert.ToInt32(num_probs_str_arr[i]);

                if (chap_probs < 1 || chap_probs > 100) return;

                num_problems_arr[i] = chap_probs;
            }

            var special_probs = 0;

            var pages = 1;

            for (var i = 0; i < num_chapters; i++)
            {
                Trace.WriteLine( $"Chapter: {i + 1}" );

                var num_chap_probs = num_problems_arr[ i ];

                Trace.WriteLine( $"Problems: {num_chap_probs}" );

                var num_chap_probs_left = num_chap_probs;

                Trace.WriteLine( $"Problems left: {num_chap_probs_left}" );

                var accum_ch_probs = 0;

                var on_last_chap_page = false;

                while (!on_last_chap_page)
                {
                    var probs_curr_pg = max_probs_per_chap >= num_chap_probs_left
                        ? num_chap_probs_left%max_probs_per_chap == 0
                            ? max_probs_per_chap
                            : num_chap_probs_left%max_probs_per_chap
                        : max_probs_per_chap;

                    Trace.WriteLine( $"Problems on current page: {probs_curr_pg}" );

                    accum_ch_probs += probs_curr_pg;

                    Trace.WriteLine( $"Total problems seen: {accum_ch_probs}" );

                    var probs_prev_pg = Math.Max(0, accum_ch_probs - probs_curr_pg);

                    Trace.WriteLine( $"Prev accum num probs: {probs_prev_pg}" );

                    num_chap_probs_left = Math.Max(0, num_chap_probs_left - max_probs_per_chap);

                    Trace.WriteLine( $"# problems left for chapter: {num_chap_probs_left}" );

                    on_last_chap_page = accum_ch_probs == num_chap_probs;

                    Trace.WriteLine( $"On last page of chapter: {on_last_chap_page} " );

                    Trace.WriteLine( $"Page: {pages}" );
                   
                    Trace.WriteLine( "Is page num between chap prev problems and curr" ); 
                    // if the current page # is between the prior chapter # probs and current chapter # probs 
                    if ( pages > probs_prev_pg && pages <= accum_ch_probs )
                    {
                        special_probs++;
                        Trace.WriteLine( $"Found a special problem, count now: {special_probs}" );
                    }

                    pages++;
                }
            }

            Trace.WriteLine("Special problems: " + special_probs);
        }
    }
}