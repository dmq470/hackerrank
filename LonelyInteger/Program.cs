﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

class Solution
{

    static int lonelyinteger( int[] a )
    {
        var set = new List<int>();

        foreach ( var item in a )
        {
            if ( set.Contains( item ) )
                set.Remove( item );
            else
                set.Add( item );
        }

        if ( set.Count == 1 ) return set[ 0 ];

        Trace.WriteLine( "Problem, current state" );

        foreach ( var i in set )
            Trace.Write( i + " " );

        return -1;
    }

    private static void Main( string[] args )
    {
        var size = Convert.ToInt32( Console.ReadLine() );

        var array = new int[size];

        var move = Console.ReadLine();

        var move_split = move?.Split( ' ' );

        Debug.Assert( move_split != null, "move_split != null" );

        for ( var i = 0; i < move_split.Length; i++ )
        {
            var item = Convert.ToInt32( move_split[ i ] );
            array[ i ] = item;
        }

        var result = lonelyinteger( array );

        Trace.WriteLine( result );
        Console.WriteLine( result );
    }
}
