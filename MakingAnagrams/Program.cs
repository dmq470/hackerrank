﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakingAnagrams
{
    class Solution
    {
        static void Main(String[] args)
        {
            var a = Console.ReadLine() ?? "";
            var b = Console.ReadLine() ?? "";

            var a_dict = new Hashtable();
            var b_dict = new Dictionary<char,int>();

            add_to_hashset( a, a_dict );
            add_to_hashset( b, b_dict );

            var seen = new HashSet<char>();

            var num_to_remove_a = hashset_check( a_dict, b_dict, seen );
            var num_to_remove_b = hashset_check( b_dict, a_dict, seen );

            Console.WriteLine( num_to_remove_a + num_to_remove_b );
        }

        private static int hashset_check( IDictionary<char, int> a_dict, IDictionary<char, int> b_dict, ISet<char> seen )
        {
            var num_to_remove = 0;

            foreach ( var a_pair in a_dict )
            {
                if ( seen.Contains( a_pair.Key ) ) continue;

                seen.Add( a_pair.Key );

                var b_val = b_dict.ContainsKey( a_pair.Key )
                    ? b_dict[ a_pair.Key ]
                    : 0;

                var diff = b_val - a_pair.Value;

                num_to_remove += Math.Abs( diff );

                Trace.WriteLine( diff );
            }
            return num_to_remove;
        }

        private static void add_to_hashset( string a, Dictionary<char, int> a_dict )
        {
            foreach ( var x in a.ToCharArray() )
            {
                a_dict[ x ] = a_dict.ContainsKey( x )
                    ? a_dict[ x ] + 1
                    : 1;
            }
        }
    }
}
