﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RansomNote
{
    /*
6 4
give me one grand today night
give one grand today

Yes

6 4
give me one grand today night
give one grand tonight

    */
    class Solution
    {
        static void Main(string[] args)
        {
            //var tokens_m = Console.ReadLine().Split( ' ' );
            //var m = tokens_m[ 0 ];
            //var n = tokens_m[ 1 ];

            //var magazine = Console.ReadLine().Split( ' ' );
            //var ransom   = Console.ReadLine().Split( ' ' );

            var magazine = "wi z ne we ebixk yvrd qrd ojckw q xe e bcco xb ieqym dwuu w dnapw achkt xqdhs nstms zmqu csqxi rim tvic arq fvjqx x su ty zl zmah y tv rkjq hpvpx ujj f i u fl iv n mnrvx tho diz k tidi gr ptkq z tho su diz yvrd dwuu dnapw xb arq xb mnrvx xe bcco qrd y ptkq rim fvjqx bcco q q wi i tidi gr mnrvx hpvpx tv f y mnrvx we fvjqx tv f wi ptkq ujj rim ebixk tho ptkq rkjq yvrd dwuu zl ujj zl qrd e ieqym";
            var ransom = "dwuu tvic y dnapw ujj tidi nstms x xe achkt x su zmqu iv zmqu xb ojckw we fvjqx tvic tv ne rkjq diz tvic we rkjq nstms zmah ieqym zmah fl xb wi tho x z ty u i gr ptkq q su tho rim tv iv iv yvrd xe qrd y dnapw q zmah arq we ieqym su zl q xb arq rkjq q e xb zl ty fvjqx ptkq ieqym qrd y wi wi nstms diz dnapw zmah q ebixk su e xb q i mnrvx wi x x tidi w ojckw bcco e tv rkjq tho";

            Console.WriteLine( usable( magazine.Split( ' ' ), ransom.Split( ' ' ) ) ? "Yes" : "No" );
        }

        private static bool usable( string[] magazine, string[] ransom )
        {
            var maga_hash = new Dictionary<string, int>();
            var rans_hash = new Dictionary<string, int>();

            foreach ( var s in magazine )
            {
                if ( maga_hash.ContainsKey( s ) )
                    maga_hash[ s ]++;
                else
                    maga_hash[s] = 1;
            }

            foreach ( var t in ransom )
            {
                if ( rans_hash.ContainsKey( t ) )
                    rans_hash[ t ]++;
                else
                    rans_hash[ t] = 1 ;
            }

            return rans_hash.All( s => maga_hash[ s.Key ] >= s.Value );
        }
    }
}
