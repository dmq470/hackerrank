﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SherlockAndArray
{
    class Program
    {
        /*

Sample Input
3
3
1 2 3
4
1 2 3 3
1
6

Sample Output
NO
YES
        
        */
        static void Main(string[] args)
        {
            int numTests;
            int arrLength;
            IList<int> arr;


            if( ! int.TryParse(Console.ReadLine(), out numTests) )
                log( "Couldn't parse");


            for (int i = 0; i < numTests; i++)
            {
                if (!int.TryParse(Console.ReadLine(), out arrLength))
                    log("Couldn't parse");

                var arrStr = Console.ReadLine().Split( ' ' );

                arr = new List<int>(arrLength);

                arrStr.ToList().ForEach(x => arr.Add( Convert.ToInt32( x ) ));

                var status = "NO";

                if (arrLength == 1)
                {
                    log( "YES" );
                    continue;
                }

                var left = arr[0];
                var right = subArraySum( arr.ToArray(), 2, arrLength -1);

                for (int j = 1; j < arrLength-1; j++)
                {
                    log("left : " + left);
                    log("right: " + right);

                    if (left == right)
                    {
                        status = "YES";
                        break;
                    }

                    left  += arr[j];
                    right -= arr[j + 1];

                    log("updated left : " + left);
                    log("updated right: " + right);
                }

                log( status );
            }


            Console.ReadLine();
            
        }

        private static int subArraySum( int[] arr, int v1, int v2)
        {
            var sum = 0;

            for (int i = v1; i <= v2; i++)
            {
                sum += arr[i];
            }

            return sum;
        }

        private static void log( string msg )
        {
            if (Debugger.IsAttached)
            {
                Trace.WriteLine( msg );
            }
            else
                Console.WriteLine( msg );
        }
    }
}
