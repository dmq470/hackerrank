﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PE1_mult_3_and_5
{
    class Program
    {
        
        static void Main( string[] args )
        {
            /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */
            var n = int.Parse( Console.ReadLine() );

            var tests = new int[n];

            for ( var j = 0; j < n; j++ )
                tests[ j ] = int.Parse( Console.ReadLine() );

            //int[] checks = {3, 6, 9, 0, 5};

            foreach (var test in tests)
            {
                long x = test - 1;

                var mult3 = 3 * seqSum( x/3 );

                var mult5 = 5 * seqSum( x/5 );

                var mult15 = 15 * seqSum( x/15 );

                long sum = mult3 + mult5 - mult15 ;

                Console.WriteLine(sum);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Now we know that sum of AP is given by
        /// S = ( n × (a+l) ) / 2
        /// here n is the number of terms, a is the starting term, and l is the last term.
        /// (usually a = 1 and l = n)
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        private static long seqSum( long l )
        {
            return ( l * ( l + 1 ) / 2 );
        }

        private static long multSum(int multiple, int limit)
        {
            long mult = 0;
            int i = 0;
            long sum = 0;
            while (mult < limit)
            {
                sum += mult;
                i++;
                mult = multiple * i;
            }
            return sum;
        }

        private static long multSub(int multiple, int limit)
        {
            long mult = 0;
            int i = 1;
            long sum = 0;
            while (mult < limit)
            {
                sum -= mult;
                i++;
                mult = multiple * i;
            }
            return sum;
        }

        private static long Sum( int start, int test )
        {
            long sum = 0;  

            for ( var x = start; x < test; x++ )
            {
                if ( x % 3 == 0 || x % 5 == 0 )
                {
                    //Console.WriteLine( $"Sum: {sum}\t \t x: {x}" );
                    sum += x;
                    //arr[ x ] = true;
                }
            }
            return sum;
        }
    }
}
