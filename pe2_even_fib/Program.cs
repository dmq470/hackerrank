﻿using System;
using System.Collections.Generic;
using System.IO;
class Solution
{
    static void Main(String[] args)
    {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */
        // ReSharper disable once AssignNullToNotNullAttribute
        int n = int.Parse(Console.ReadLine());

        var tests = new long[n];

        for (long j = 0; j < n; j++)
            // ReSharper disable once AssignNullToNotNullAttribute
            tests[j] = long.Parse(Console.ReadLine());

        foreach ( long test in tests )
        {
            long sum = 0;
            long curr = 1;
            long prev = 0;

            while (curr < test)
            {
                if (curr % 2 == 0)
                    sum += curr;

                long temp = prev;
                prev = curr;
                curr += temp;
            }

            Console.WriteLine( sum );
        }

        Console.ReadLine();
    }
}